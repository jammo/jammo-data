svn 654
Picture of ud:
The copyright holder of this file allows anyone to use it for any purpose, provided that specific permission is granted and name is credited (Viken Najarian, Najouds@aol.com).
http://en.wikipedia.org/wiki/File:Oud.jpg

Picture of drum kit:
This file is licensed under the Creative Commons Attribution 3.0 Unported License. In short: you are free to distribute and modify the file as long as you attribute its author(s) or licensor(s). 
http://en.wikipedia.org/wiki/File:Drum_set.svg

Picture of flute:
This image is in the public domain because its copyright has expired in the United States and those countries with a copyright term of no more than the life of the author plus 100 years.
http://en.wikipedia.org/wiki/File:1917_flute.jpg

Hourglass (hourglass.png) taken from Pandora Minimenu/Jeff Mitchell, 2010 Released under GPL v2

Save-project (save_project.png) By: warszawianka from:
http://www.openclipart.org/detail/32245 "Tango Project icons are Public Domain"

Microphone (for avatar). Public Domain
https://secure.wikimedia.org/wikipedia/commons/wiki/File:Microphone.svg


All other pictures are part of JamMo and licensed under GPL v2.0.



