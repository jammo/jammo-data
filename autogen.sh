#!/bin/sh

touch AUTHORS INSTALL NEWS README ChangeLog install-sh missing

aclocal
automake -ac
autoconf

exit 0
